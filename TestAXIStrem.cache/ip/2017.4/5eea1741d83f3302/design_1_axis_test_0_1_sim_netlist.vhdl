-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
-- Date        : Sun May  6 23:21:08 2018
-- Host        : MaratBigLinux running 64-bit Ubuntu 16.04.4 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_axis_test_0_1_sim_netlist.vhdl
-- Design      : design_1_axis_test_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_add_one is
  port (
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_aclk : in STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_add_one;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_add_one is
  signal \result0[0]_i_1_n_0\ : STD_LOGIC;
  signal \result0[1]_i_1_n_0\ : STD_LOGIC;
  signal \result0[2]_i_1_n_0\ : STD_LOGIC;
  signal \result0[3]_i_1_n_0\ : STD_LOGIC;
  signal \result0[4]_i_1_n_0\ : STD_LOGIC;
  signal \result0[5]_i_1_n_0\ : STD_LOGIC;
  signal \result0[6]_i_1_n_0\ : STD_LOGIC;
  signal \result0[7]_i_1_n_0\ : STD_LOGIC;
  signal \result0[7]_i_2_n_0\ : STD_LOGIC;
  signal \result1[0]_i_1_n_0\ : STD_LOGIC;
  signal \result1[1]_i_1_n_0\ : STD_LOGIC;
  signal \result1[2]_i_1_n_0\ : STD_LOGIC;
  signal \result1[3]_i_1_n_0\ : STD_LOGIC;
  signal \result1[4]_i_1_n_0\ : STD_LOGIC;
  signal \result1[5]_i_1_n_0\ : STD_LOGIC;
  signal \result1[6]_i_1_n_0\ : STD_LOGIC;
  signal \result1[7]_i_1_n_0\ : STD_LOGIC;
  signal \result1[7]_i_2_n_0\ : STD_LOGIC;
  signal \result2[0]_i_1_n_0\ : STD_LOGIC;
  signal \result2[1]_i_1_n_0\ : STD_LOGIC;
  signal \result2[2]_i_1_n_0\ : STD_LOGIC;
  signal \result2[3]_i_1_n_0\ : STD_LOGIC;
  signal \result2[4]_i_1_n_0\ : STD_LOGIC;
  signal \result2[5]_i_1_n_0\ : STD_LOGIC;
  signal \result2[6]_i_1_n_0\ : STD_LOGIC;
  signal \result2[7]_i_1_n_0\ : STD_LOGIC;
  signal \result2[7]_i_2_n_0\ : STD_LOGIC;
  signal \result3[0]_i_1_n_0\ : STD_LOGIC;
  signal \result3[1]_i_1_n_0\ : STD_LOGIC;
  signal \result3[2]_i_1_n_0\ : STD_LOGIC;
  signal \result3[3]_i_1_n_0\ : STD_LOGIC;
  signal \result3[4]_i_1_n_0\ : STD_LOGIC;
  signal \result3[5]_i_1_n_0\ : STD_LOGIC;
  signal \result3[6]_i_1_n_0\ : STD_LOGIC;
  signal \result3[7]_i_1_n_0\ : STD_LOGIC;
  signal \result3[7]_i_2_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \result0[1]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \result0[2]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \result0[3]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \result0[4]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \result0[6]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \result0[7]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \result1[1]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \result1[2]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \result1[3]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \result1[4]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \result1[6]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \result1[7]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \result2[1]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \result2[2]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \result2[3]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \result2[4]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \result2[6]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \result2[7]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \result3[1]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \result3[2]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \result3[3]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \result3[4]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \result3[6]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \result3[7]_i_1\ : label is "soft_lutpair17";
begin
\result0[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axis_tdata(0),
      O => \result0[0]_i_1_n_0\
    );
\result0[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(0),
      I1 => s00_axis_tdata(1),
      O => \result0[1]_i_1_n_0\
    );
\result0[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => s00_axis_tdata(0),
      I1 => s00_axis_tdata(1),
      I2 => s00_axis_tdata(2),
      O => \result0[2]_i_1_n_0\
    );
\result0[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => s00_axis_tdata(1),
      I1 => s00_axis_tdata(0),
      I2 => s00_axis_tdata(2),
      I3 => s00_axis_tdata(3),
      O => \result0[3]_i_1_n_0\
    );
\result0[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => s00_axis_tdata(2),
      I1 => s00_axis_tdata(0),
      I2 => s00_axis_tdata(1),
      I3 => s00_axis_tdata(3),
      I4 => s00_axis_tdata(4),
      O => \result0[4]_i_1_n_0\
    );
\result0[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => s00_axis_tdata(3),
      I1 => s00_axis_tdata(1),
      I2 => s00_axis_tdata(0),
      I3 => s00_axis_tdata(2),
      I4 => s00_axis_tdata(4),
      I5 => s00_axis_tdata(5),
      O => \result0[5]_i_1_n_0\
    );
\result0[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \result0[7]_i_2_n_0\,
      I1 => s00_axis_tdata(6),
      O => \result0[6]_i_1_n_0\
    );
\result0[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \result0[7]_i_2_n_0\,
      I1 => s00_axis_tdata(6),
      I2 => s00_axis_tdata(7),
      O => \result0[7]_i_1_n_0\
    );
\result0[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => s00_axis_tdata(5),
      I1 => s00_axis_tdata(3),
      I2 => s00_axis_tdata(1),
      I3 => s00_axis_tdata(0),
      I4 => s00_axis_tdata(2),
      I5 => s00_axis_tdata(4),
      O => \result0[7]_i_2_n_0\
    );
\result0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result0[0]_i_1_n_0\,
      Q => Q(0),
      R => '0'
    );
\result0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result0[1]_i_1_n_0\,
      Q => Q(1),
      R => '0'
    );
\result0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result0[2]_i_1_n_0\,
      Q => Q(2),
      R => '0'
    );
\result0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result0[3]_i_1_n_0\,
      Q => Q(3),
      R => '0'
    );
\result0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result0[4]_i_1_n_0\,
      Q => Q(4),
      R => '0'
    );
\result0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result0[5]_i_1_n_0\,
      Q => Q(5),
      R => '0'
    );
\result0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result0[6]_i_1_n_0\,
      Q => Q(6),
      R => '0'
    );
\result0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result0[7]_i_1_n_0\,
      Q => Q(7),
      R => '0'
    );
\result1[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axis_tdata(8),
      O => \result1[0]_i_1_n_0\
    );
\result1[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(8),
      I1 => s00_axis_tdata(9),
      O => \result1[1]_i_1_n_0\
    );
\result1[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => s00_axis_tdata(8),
      I1 => s00_axis_tdata(9),
      I2 => s00_axis_tdata(10),
      O => \result1[2]_i_1_n_0\
    );
\result1[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => s00_axis_tdata(9),
      I1 => s00_axis_tdata(8),
      I2 => s00_axis_tdata(10),
      I3 => s00_axis_tdata(11),
      O => \result1[3]_i_1_n_0\
    );
\result1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => s00_axis_tdata(10),
      I1 => s00_axis_tdata(8),
      I2 => s00_axis_tdata(9),
      I3 => s00_axis_tdata(11),
      I4 => s00_axis_tdata(12),
      O => \result1[4]_i_1_n_0\
    );
\result1[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => s00_axis_tdata(11),
      I1 => s00_axis_tdata(9),
      I2 => s00_axis_tdata(8),
      I3 => s00_axis_tdata(10),
      I4 => s00_axis_tdata(12),
      I5 => s00_axis_tdata(13),
      O => \result1[5]_i_1_n_0\
    );
\result1[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \result1[7]_i_2_n_0\,
      I1 => s00_axis_tdata(14),
      O => \result1[6]_i_1_n_0\
    );
\result1[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \result1[7]_i_2_n_0\,
      I1 => s00_axis_tdata(14),
      I2 => s00_axis_tdata(15),
      O => \result1[7]_i_1_n_0\
    );
\result1[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => s00_axis_tdata(13),
      I1 => s00_axis_tdata(11),
      I2 => s00_axis_tdata(9),
      I3 => s00_axis_tdata(8),
      I4 => s00_axis_tdata(10),
      I5 => s00_axis_tdata(12),
      O => \result1[7]_i_2_n_0\
    );
\result1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result1[0]_i_1_n_0\,
      Q => Q(8),
      R => '0'
    );
\result1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result1[1]_i_1_n_0\,
      Q => Q(9),
      R => '0'
    );
\result1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result1[2]_i_1_n_0\,
      Q => Q(10),
      R => '0'
    );
\result1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result1[3]_i_1_n_0\,
      Q => Q(11),
      R => '0'
    );
\result1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result1[4]_i_1_n_0\,
      Q => Q(12),
      R => '0'
    );
\result1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result1[5]_i_1_n_0\,
      Q => Q(13),
      R => '0'
    );
\result1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result1[6]_i_1_n_0\,
      Q => Q(14),
      R => '0'
    );
\result1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result1[7]_i_1_n_0\,
      Q => Q(15),
      R => '0'
    );
\result2[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axis_tdata(16),
      O => \result2[0]_i_1_n_0\
    );
\result2[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(16),
      I1 => s00_axis_tdata(17),
      O => \result2[1]_i_1_n_0\
    );
\result2[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => s00_axis_tdata(16),
      I1 => s00_axis_tdata(17),
      I2 => s00_axis_tdata(18),
      O => \result2[2]_i_1_n_0\
    );
\result2[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => s00_axis_tdata(17),
      I1 => s00_axis_tdata(16),
      I2 => s00_axis_tdata(18),
      I3 => s00_axis_tdata(19),
      O => \result2[3]_i_1_n_0\
    );
\result2[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => s00_axis_tdata(18),
      I1 => s00_axis_tdata(16),
      I2 => s00_axis_tdata(17),
      I3 => s00_axis_tdata(19),
      I4 => s00_axis_tdata(20),
      O => \result2[4]_i_1_n_0\
    );
\result2[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => s00_axis_tdata(19),
      I1 => s00_axis_tdata(17),
      I2 => s00_axis_tdata(16),
      I3 => s00_axis_tdata(18),
      I4 => s00_axis_tdata(20),
      I5 => s00_axis_tdata(21),
      O => \result2[5]_i_1_n_0\
    );
\result2[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \result2[7]_i_2_n_0\,
      I1 => s00_axis_tdata(22),
      O => \result2[6]_i_1_n_0\
    );
\result2[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \result2[7]_i_2_n_0\,
      I1 => s00_axis_tdata(22),
      I2 => s00_axis_tdata(23),
      O => \result2[7]_i_1_n_0\
    );
\result2[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => s00_axis_tdata(21),
      I1 => s00_axis_tdata(19),
      I2 => s00_axis_tdata(17),
      I3 => s00_axis_tdata(16),
      I4 => s00_axis_tdata(18),
      I5 => s00_axis_tdata(20),
      O => \result2[7]_i_2_n_0\
    );
\result2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result2[0]_i_1_n_0\,
      Q => Q(16),
      R => '0'
    );
\result2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result2[1]_i_1_n_0\,
      Q => Q(17),
      R => '0'
    );
\result2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result2[2]_i_1_n_0\,
      Q => Q(18),
      R => '0'
    );
\result2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result2[3]_i_1_n_0\,
      Q => Q(19),
      R => '0'
    );
\result2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result2[4]_i_1_n_0\,
      Q => Q(20),
      R => '0'
    );
\result2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result2[5]_i_1_n_0\,
      Q => Q(21),
      R => '0'
    );
\result2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result2[6]_i_1_n_0\,
      Q => Q(22),
      R => '0'
    );
\result2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result2[7]_i_1_n_0\,
      Q => Q(23),
      R => '0'
    );
\result3[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axis_tdata(24),
      O => \result3[0]_i_1_n_0\
    );
\result3[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(24),
      I1 => s00_axis_tdata(25),
      O => \result3[1]_i_1_n_0\
    );
\result3[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => s00_axis_tdata(24),
      I1 => s00_axis_tdata(25),
      I2 => s00_axis_tdata(26),
      O => \result3[2]_i_1_n_0\
    );
\result3[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => s00_axis_tdata(25),
      I1 => s00_axis_tdata(24),
      I2 => s00_axis_tdata(26),
      I3 => s00_axis_tdata(27),
      O => \result3[3]_i_1_n_0\
    );
\result3[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => s00_axis_tdata(26),
      I1 => s00_axis_tdata(24),
      I2 => s00_axis_tdata(25),
      I3 => s00_axis_tdata(27),
      I4 => s00_axis_tdata(28),
      O => \result3[4]_i_1_n_0\
    );
\result3[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => s00_axis_tdata(27),
      I1 => s00_axis_tdata(25),
      I2 => s00_axis_tdata(24),
      I3 => s00_axis_tdata(26),
      I4 => s00_axis_tdata(28),
      I5 => s00_axis_tdata(29),
      O => \result3[5]_i_1_n_0\
    );
\result3[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \result3[7]_i_2_n_0\,
      I1 => s00_axis_tdata(30),
      O => \result3[6]_i_1_n_0\
    );
\result3[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \result3[7]_i_2_n_0\,
      I1 => s00_axis_tdata(30),
      I2 => s00_axis_tdata(31),
      O => \result3[7]_i_1_n_0\
    );
\result3[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => s00_axis_tdata(29),
      I1 => s00_axis_tdata(27),
      I2 => s00_axis_tdata(25),
      I3 => s00_axis_tdata(24),
      I4 => s00_axis_tdata(26),
      I5 => s00_axis_tdata(28),
      O => \result3[7]_i_2_n_0\
    );
\result3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result3[0]_i_1_n_0\,
      Q => Q(24),
      R => '0'
    );
\result3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result3[1]_i_1_n_0\,
      Q => Q(25),
      R => '0'
    );
\result3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result3[2]_i_1_n_0\,
      Q => Q(26),
      R => '0'
    );
\result3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result3[3]_i_1_n_0\,
      Q => Q(27),
      R => '0'
    );
\result3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result3[4]_i_1_n_0\,
      Q => Q(28),
      R => '0'
    );
\result3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result3[5]_i_1_n_0\,
      Q => Q(29),
      R => '0'
    );
\result3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result3[6]_i_1_n_0\,
      Q => Q(30),
      R => '0'
    );
\result3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \result3[7]_i_1_n_0\,
      Q => Q(31),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_M00_AXIS is
  port (
    m00_axis_tvalid : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aresetn : in STD_LOGIC;
    m00_axis_tready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_M00_AXIS;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_M00_AXIS is
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal axis_tlast : STD_LOGIC;
  signal axis_tvalid : STD_LOGIC;
  signal count0 : STD_LOGIC;
  signal \count[3]_i_1_n_0\ : STD_LOGIC;
  signal mst_exec_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \mst_exec_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \mst_exec_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \mst_exec_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal read_pointer : STD_LOGIC;
  signal \read_pointer[0]_i_1_n_0\ : STD_LOGIC;
  signal \read_pointer[1]_i_1_n_0\ : STD_LOGIC;
  signal \read_pointer[2]_i_1_n_0\ : STD_LOGIC;
  signal \read_pointer[3]_i_2_n_0\ : STD_LOGIC;
  signal \read_pointer_reg__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal sel0 : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \stream_data_out[0]_i_1_n_0\ : STD_LOGIC;
  signal \stream_data_out[1]_i_1_n_0\ : STD_LOGIC;
  signal \stream_data_out[2]_i_1_n_0\ : STD_LOGIC;
  signal \stream_data_out[31]_i_1_n_0\ : STD_LOGIC;
  signal \stream_data_out[31]_i_2_n_0\ : STD_LOGIC;
  signal \stream_data_out[3]_i_1_n_0\ : STD_LOGIC;
  signal \stream_data_out[4]_i_1_n_0\ : STD_LOGIC;
  signal tx_done_i_1_n_0 : STD_LOGIC;
  signal tx_done_i_2_n_0 : STD_LOGIC;
  signal tx_done_reg_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count[0]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \count[1]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \count[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \count[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \count[4]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \mst_exec_state[1]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \read_pointer[0]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \read_pointer[1]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \read_pointer[2]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \read_pointer[3]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \stream_data_out[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \stream_data_out[1]_i_1\ : label is "soft_lutpair1";
begin
  SR(0) <= \^sr\(0);
axis_tlast_delay_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \read_pointer_reg__0\(3),
      I1 => \read_pointer_reg__0\(2),
      I2 => \read_pointer_reg__0\(0),
      I3 => \read_pointer_reg__0\(1),
      O => axis_tlast
    );
axis_tlast_delay_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => axis_tlast,
      Q => m00_axis_tlast,
      R => \^sr\(0)
    );
axis_tvalid_delay_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => mst_exec_state(0),
      I1 => mst_exec_state(1),
      I2 => \read_pointer_reg__0\(3),
      O => axis_tvalid
    );
axis_tvalid_delay_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => axis_tvalid,
      Q => m00_axis_tvalid,
      R => \^sr\(0)
    );
\count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sel0(0),
      O => \p_0_in__0\(0)
    );
\count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => sel0(0),
      I1 => sel0(1),
      O => \p_0_in__0\(1)
    );
\count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => sel0(2),
      I1 => sel0(0),
      I2 => sel0(1),
      O => \p_0_in__0\(2)
    );
\count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => sel0(2),
      I1 => sel0(1),
      I2 => sel0(0),
      I3 => sel0(3),
      O => \count[3]_i_1_n_0\
    );
\count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"44044444"
    )
        port map (
      I0 => mst_exec_state(1),
      I1 => mst_exec_state(0),
      I2 => sel0(3),
      I3 => \mst_exec_state[1]_i_2_n_0\,
      I4 => sel0(4),
      O => count0
    );
\count[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => sel0(0),
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => sel0(3),
      I4 => sel0(4),
      O => \p_0_in__0\(4)
    );
\count_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => count0,
      D => \p_0_in__0\(0),
      Q => sel0(0),
      R => \^sr\(0)
    );
\count_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => count0,
      D => \p_0_in__0\(1),
      Q => sel0(1),
      R => \^sr\(0)
    );
\count_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => count0,
      D => \p_0_in__0\(2),
      Q => sel0(2),
      R => \^sr\(0)
    );
\count_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => count0,
      D => \count[3]_i_1_n_0\,
      Q => sel0(3),
      R => \^sr\(0)
    );
\count_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => count0,
      D => \p_0_in__0\(4),
      Q => sel0(4),
      R => \^sr\(0)
    );
\mst_exec_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00BFFF"
    )
        port map (
      I0 => \mst_exec_state[1]_i_2_n_0\,
      I1 => sel0(4),
      I2 => sel0(3),
      I3 => mst_exec_state(0),
      I4 => mst_exec_state(1),
      O => \mst_exec_state[0]_i_1_n_0\
    );
\mst_exec_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF555530000000"
    )
        port map (
      I0 => tx_done_reg_n_0,
      I1 => \mst_exec_state[1]_i_2_n_0\,
      I2 => sel0(4),
      I3 => sel0(3),
      I4 => mst_exec_state(0),
      I5 => mst_exec_state(1),
      O => \mst_exec_state[1]_i_1_n_0\
    );
\mst_exec_state[1]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => sel0(0),
      I1 => sel0(1),
      I2 => sel0(2),
      O => \mst_exec_state[1]_i_2_n_0\
    );
mst_exec_state_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => m00_axis_aresetn,
      O => \^sr\(0)
    );
\mst_exec_state_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \mst_exec_state[0]_i_1_n_0\,
      Q => mst_exec_state(0),
      R => \^sr\(0)
    );
\mst_exec_state_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \mst_exec_state[1]_i_1_n_0\,
      Q => mst_exec_state(1),
      R => \^sr\(0)
    );
\read_pointer[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \read_pointer_reg__0\(0),
      O => \read_pointer[0]_i_1_n_0\
    );
\read_pointer[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_pointer_reg__0\(0),
      I1 => \read_pointer_reg__0\(1),
      O => \read_pointer[1]_i_1_n_0\
    );
\read_pointer[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \read_pointer_reg__0\(0),
      I1 => \read_pointer_reg__0\(1),
      I2 => \read_pointer_reg__0\(2),
      O => \read_pointer[2]_i_1_n_0\
    );
\read_pointer[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => m00_axis_tready,
      I1 => \read_pointer_reg__0\(3),
      I2 => mst_exec_state(1),
      I3 => mst_exec_state(0),
      O => read_pointer
    );
\read_pointer[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \read_pointer_reg__0\(1),
      I1 => \read_pointer_reg__0\(0),
      I2 => \read_pointer_reg__0\(2),
      O => \read_pointer[3]_i_2_n_0\
    );
\read_pointer_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => read_pointer,
      D => \read_pointer[0]_i_1_n_0\,
      Q => \read_pointer_reg__0\(0),
      R => \^sr\(0)
    );
\read_pointer_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => read_pointer,
      D => \read_pointer[1]_i_1_n_0\,
      Q => \read_pointer_reg__0\(1),
      R => \^sr\(0)
    );
\read_pointer_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => read_pointer,
      D => \read_pointer[2]_i_1_n_0\,
      Q => \read_pointer_reg__0\(2),
      R => \^sr\(0)
    );
\read_pointer_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => read_pointer,
      D => \read_pointer[3]_i_2_n_0\,
      Q => \read_pointer_reg__0\(3),
      R => \^sr\(0)
    );
\stream_data_out[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => \read_pointer_reg__0\(0),
      I1 => m00_axis_aresetn,
      I2 => Q(0),
      O => \stream_data_out[0]_i_1_n_0\
    );
\stream_data_out[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => \read_pointer_reg__0\(0),
      I1 => \read_pointer_reg__0\(1),
      I2 => m00_axis_aresetn,
      I3 => Q(1),
      O => \stream_data_out[1]_i_1_n_0\
    );
\stream_data_out[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"78FF7800"
    )
        port map (
      I0 => \read_pointer_reg__0\(0),
      I1 => \read_pointer_reg__0\(1),
      I2 => \read_pointer_reg__0\(2),
      I3 => m00_axis_aresetn,
      I4 => Q(2),
      O => \stream_data_out[2]_i_1_n_0\
    );
\stream_data_out[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000800"
    )
        port map (
      I0 => m00_axis_aresetn,
      I1 => m00_axis_tready,
      I2 => \read_pointer_reg__0\(3),
      I3 => mst_exec_state(1),
      I4 => mst_exec_state(0),
      O => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0400FFFF"
    )
        port map (
      I0 => mst_exec_state(0),
      I1 => mst_exec_state(1),
      I2 => \read_pointer_reg__0\(3),
      I3 => m00_axis_tready,
      I4 => m00_axis_aresetn,
      O => \stream_data_out[31]_i_2_n_0\
    );
\stream_data_out[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F80FFFF7F800000"
    )
        port map (
      I0 => \read_pointer_reg__0\(1),
      I1 => \read_pointer_reg__0\(0),
      I2 => \read_pointer_reg__0\(2),
      I3 => \read_pointer_reg__0\(3),
      I4 => m00_axis_aresetn,
      I5 => Q(3),
      O => \stream_data_out[3]_i_1_n_0\
    );
\stream_data_out[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000FFFF80000000"
    )
        port map (
      I0 => \read_pointer_reg__0\(2),
      I1 => \read_pointer_reg__0\(0),
      I2 => \read_pointer_reg__0\(1),
      I3 => \read_pointer_reg__0\(3),
      I4 => m00_axis_aresetn,
      I5 => Q(4),
      O => \stream_data_out[4]_i_1_n_0\
    );
\stream_data_out_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => \stream_data_out[0]_i_1_n_0\,
      Q => m00_axis_tdata(0),
      R => '0'
    );
\stream_data_out_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(10),
      Q => m00_axis_tdata(10),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(11),
      Q => m00_axis_tdata(11),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(12),
      Q => m00_axis_tdata(12),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(13),
      Q => m00_axis_tdata(13),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(14),
      Q => m00_axis_tdata(14),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(15),
      Q => m00_axis_tdata(15),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(16),
      Q => m00_axis_tdata(16),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(17),
      Q => m00_axis_tdata(17),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(18),
      Q => m00_axis_tdata(18),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(19),
      Q => m00_axis_tdata(19),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => \stream_data_out[1]_i_1_n_0\,
      Q => m00_axis_tdata(1),
      R => '0'
    );
\stream_data_out_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(20),
      Q => m00_axis_tdata(20),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(21),
      Q => m00_axis_tdata(21),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(22),
      Q => m00_axis_tdata(22),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(23),
      Q => m00_axis_tdata(23),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(24),
      Q => m00_axis_tdata(24),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(25),
      Q => m00_axis_tdata(25),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(26),
      Q => m00_axis_tdata(26),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(27),
      Q => m00_axis_tdata(27),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(28),
      Q => m00_axis_tdata(28),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(29),
      Q => m00_axis_tdata(29),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => \stream_data_out[2]_i_1_n_0\,
      Q => m00_axis_tdata(2),
      R => '0'
    );
\stream_data_out_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(30),
      Q => m00_axis_tdata(30),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(31),
      Q => m00_axis_tdata(31),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => \stream_data_out[3]_i_1_n_0\,
      Q => m00_axis_tdata(3),
      R => '0'
    );
\stream_data_out_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => \stream_data_out[4]_i_1_n_0\,
      Q => m00_axis_tdata(4),
      R => '0'
    );
\stream_data_out_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(5),
      Q => m00_axis_tdata(5),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(6),
      Q => m00_axis_tdata(6),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(7),
      Q => m00_axis_tdata(7),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(8),
      Q => m00_axis_tdata(8),
      R => \stream_data_out[31]_i_1_n_0\
    );
\stream_data_out_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \stream_data_out[31]_i_2_n_0\,
      D => Q(9),
      Q => m00_axis_tdata(9),
      R => \stream_data_out[31]_i_1_n_0\
    );
tx_done_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA8AAAAA"
    )
        port map (
      I0 => tx_done_i_2_n_0,
      I1 => mst_exec_state(0),
      I2 => mst_exec_state(1),
      I3 => \read_pointer_reg__0\(3),
      I4 => m00_axis_tready,
      O => tx_done_i_1_n_0
    );
tx_done_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAABAAAA00000000"
    )
        port map (
      I0 => tx_done_reg_n_0,
      I1 => \read_pointer_reg__0\(1),
      I2 => \read_pointer_reg__0\(0),
      I3 => \read_pointer_reg__0\(2),
      I4 => \read_pointer_reg__0\(3),
      I5 => m00_axis_aresetn,
      O => tx_done_i_2_n_0
    );
tx_done_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => tx_done_i_1_n_0,
      Q => tx_done_reg_n_0,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_S00_AXIS is
  port (
    s00_axis_tready : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_aclk : in STD_LOGIC;
    s00_axis_tlast : in STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_S00_AXIS;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_S00_AXIS is
  signal fifo_wren : STD_LOGIC;
  signal mst_exec_state_i_2_n_0 : STD_LOGIC;
  signal \^s00_axis_tready\ : STD_LOGIC;
  signal write_pointer : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \write_pointer[0]_i_1_n_0\ : STD_LOGIC;
  signal \write_pointer[1]_i_1_n_0\ : STD_LOGIC;
  signal \write_pointer[2]_i_1_n_0\ : STD_LOGIC;
  signal writes_done : STD_LOGIC;
  signal writes_done_i_1_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \write_pointer[0]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \write_pointer[1]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \write_pointer[2]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of writes_done_i_2 : label is "soft_lutpair19";
begin
  s00_axis_tready <= \^s00_axis_tready\;
add_one_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_add_one
     port map (
      Q(31 downto 0) => Q(31 downto 0),
      s00_axis_aclk => s00_axis_aclk,
      s00_axis_tdata(31 downto 0) => s00_axis_tdata(31 downto 0)
    );
mst_exec_state_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => writes_done,
      I1 => \^s00_axis_tready\,
      I2 => s00_axis_tvalid,
      O => mst_exec_state_i_2_n_0
    );
mst_exec_state_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => mst_exec_state_i_2_n_0,
      Q => \^s00_axis_tready\,
      R => SR(0)
    );
\write_pointer[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^s00_axis_tready\,
      I1 => s00_axis_tvalid,
      I2 => write_pointer(0),
      O => \write_pointer[0]_i_1_n_0\
    );
\write_pointer[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => write_pointer(0),
      I1 => s00_axis_tvalid,
      I2 => \^s00_axis_tready\,
      I3 => write_pointer(1),
      O => \write_pointer[1]_i_1_n_0\
    );
\write_pointer[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => write_pointer(0),
      I1 => write_pointer(1),
      I2 => s00_axis_tvalid,
      I3 => \^s00_axis_tready\,
      I4 => write_pointer(2),
      O => \write_pointer[2]_i_1_n_0\
    );
\write_pointer_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \write_pointer[0]_i_1_n_0\,
      Q => write_pointer(0),
      R => SR(0)
    );
\write_pointer_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \write_pointer[1]_i_1_n_0\,
      Q => write_pointer(1),
      R => SR(0)
    );
\write_pointer_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => \write_pointer[2]_i_1_n_0\,
      Q => write_pointer(2),
      R => SR(0)
    );
writes_done_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF80FFFFFF80FF80"
    )
        port map (
      I0 => write_pointer(1),
      I1 => write_pointer(0),
      I2 => write_pointer(2),
      I3 => s00_axis_tlast,
      I4 => fifo_wren,
      I5 => writes_done,
      O => writes_done_i_1_n_0
    );
writes_done_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s00_axis_tvalid,
      I1 => \^s00_axis_tready\,
      O => fifo_wren
    );
writes_done_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => writes_done_i_1_n_0,
      Q => writes_done,
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0 is
  port (
    s00_axis_tready : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    s00_axis_aclk : in STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_tvalid : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    s00_axis_tlast : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0 is
  signal p_0_in : STD_LOGIC;
  signal procWireM : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
axis_test_v1_0_M00_AXIS_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_M00_AXIS
     port map (
      Q(31 downto 0) => procWireM(31 downto 0),
      SR(0) => p_0_in,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tready => m00_axis_tready,
      m00_axis_tvalid => m00_axis_tvalid,
      s00_axis_aclk => s00_axis_aclk
    );
axis_test_v1_0_S00_AXIS_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_S00_AXIS
     port map (
      Q(31 downto 0) => procWireM(31 downto 0),
      SR(0) => p_0_in,
      s00_axis_aclk => s00_axis_aclk,
      s00_axis_tdata(31 downto 0) => s00_axis_tdata(31 downto 0),
      s00_axis_tlast => s00_axis_tlast,
      s00_axis_tready => s00_axis_tready,
      s00_axis_tvalid => s00_axis_tvalid
    );
\procDataM_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(0),
      R => '0'
    );
\procDataM_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(10),
      R => '0'
    );
\procDataM_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(11),
      R => '0'
    );
\procDataM_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(12),
      R => '0'
    );
\procDataM_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(13),
      R => '0'
    );
\procDataM_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(14),
      R => '0'
    );
\procDataM_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(15),
      R => '0'
    );
\procDataM_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(16),
      R => '0'
    );
\procDataM_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(17),
      R => '0'
    );
\procDataM_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(18),
      R => '0'
    );
\procDataM_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(19),
      R => '0'
    );
\procDataM_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(1),
      R => '0'
    );
\procDataM_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(20),
      R => '0'
    );
\procDataM_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(21),
      R => '0'
    );
\procDataM_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(22),
      R => '0'
    );
\procDataM_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(23),
      R => '0'
    );
\procDataM_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(24),
      R => '0'
    );
\procDataM_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(25),
      R => '0'
    );
\procDataM_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(26),
      R => '0'
    );
\procDataM_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(27),
      R => '0'
    );
\procDataM_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(28),
      R => '0'
    );
\procDataM_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(29),
      R => '0'
    );
\procDataM_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(2),
      R => '0'
    );
\procDataM_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(30),
      R => '0'
    );
\procDataM_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(31),
      R => '0'
    );
\procDataM_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(3),
      R => '0'
    );
\procDataM_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(4),
      R => '0'
    );
\procDataM_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(5),
      R => '0'
    );
\procDataM_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(6),
      R => '0'
    );
\procDataM_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(7),
      R => '0'
    );
\procDataM_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(8),
      R => '0'
    );
\procDataM_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => '1',
      D => '0',
      Q => procWireM(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_tstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axis_tlast : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC;
    s00_axis_tready : out STD_LOGIC;
    s00_axis_aclk : in STD_LOGIC;
    s00_axis_aresetn : in STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_axis_test_0_1,axis_test_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "axis_test_v1_0,Vivado 2017.4";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const1>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of s00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXIS_CLK CLK";
  attribute X_INTERFACE_PARAMETER of s00_axis_aclk : signal is "XIL_INTERFACENAME S00_AXIS_CLK, ASSOCIATED_BUSIF S00_AXIS, ASSOCIATED_RESET s00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0";
  attribute X_INTERFACE_INFO of s00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXIS_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axis_aresetn : signal is "XIL_INTERFACENAME S00_AXIS_RST, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of s00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TLAST";
  attribute X_INTERFACE_INFO of s00_axis_tready : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of s00_axis_tready : signal is "XIL_INTERFACENAME S00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of s00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TVALID";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TDATA";
  attribute X_INTERFACE_INFO of s00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TSTRB";
begin
  m00_axis_aresetn <= s00_axis_aresetn;
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
  s00_axis_aclk <= m00_axis_aclk;
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0
     port map (
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tready => m00_axis_tready,
      m00_axis_tvalid => m00_axis_tvalid,
      s00_axis_aclk => s00_axis_aclk,
      s00_axis_tdata(31 downto 0) => s00_axis_tdata(31 downto 0),
      s00_axis_tlast => s00_axis_tlast,
      s00_axis_tready => s00_axis_tready,
      s00_axis_tvalid => s00_axis_tvalid
    );
end STRUCTURE;

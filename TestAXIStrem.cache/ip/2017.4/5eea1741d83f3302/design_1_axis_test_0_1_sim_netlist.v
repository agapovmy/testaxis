// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Sun May  6 23:21:08 2018
// Host        : MaratBigLinux running 64-bit Ubuntu 16.04.4 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_axis_test_0_1_sim_netlist.v
// Design      : design_1_axis_test_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_add_one
   (Q,
    s00_axis_aclk,
    s00_axis_tdata);
  output [31:0]Q;
  input s00_axis_aclk;
  input [31:0]s00_axis_tdata;

  wire [31:0]Q;
  wire \result0[0]_i_1_n_0 ;
  wire \result0[1]_i_1_n_0 ;
  wire \result0[2]_i_1_n_0 ;
  wire \result0[3]_i_1_n_0 ;
  wire \result0[4]_i_1_n_0 ;
  wire \result0[5]_i_1_n_0 ;
  wire \result0[6]_i_1_n_0 ;
  wire \result0[7]_i_1_n_0 ;
  wire \result0[7]_i_2_n_0 ;
  wire \result1[0]_i_1_n_0 ;
  wire \result1[1]_i_1_n_0 ;
  wire \result1[2]_i_1_n_0 ;
  wire \result1[3]_i_1_n_0 ;
  wire \result1[4]_i_1_n_0 ;
  wire \result1[5]_i_1_n_0 ;
  wire \result1[6]_i_1_n_0 ;
  wire \result1[7]_i_1_n_0 ;
  wire \result1[7]_i_2_n_0 ;
  wire \result2[0]_i_1_n_0 ;
  wire \result2[1]_i_1_n_0 ;
  wire \result2[2]_i_1_n_0 ;
  wire \result2[3]_i_1_n_0 ;
  wire \result2[4]_i_1_n_0 ;
  wire \result2[5]_i_1_n_0 ;
  wire \result2[6]_i_1_n_0 ;
  wire \result2[7]_i_1_n_0 ;
  wire \result2[7]_i_2_n_0 ;
  wire \result3[0]_i_1_n_0 ;
  wire \result3[1]_i_1_n_0 ;
  wire \result3[2]_i_1_n_0 ;
  wire \result3[3]_i_1_n_0 ;
  wire \result3[4]_i_1_n_0 ;
  wire \result3[5]_i_1_n_0 ;
  wire \result3[6]_i_1_n_0 ;
  wire \result3[7]_i_1_n_0 ;
  wire \result3[7]_i_2_n_0 ;
  wire s00_axis_aclk;
  wire [31:0]s00_axis_tdata;

  LUT1 #(
    .INIT(2'h1)) 
    \result0[0]_i_1 
       (.I0(s00_axis_tdata[0]),
        .O(\result0[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \result0[1]_i_1 
       (.I0(s00_axis_tdata[0]),
        .I1(s00_axis_tdata[1]),
        .O(\result0[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \result0[2]_i_1 
       (.I0(s00_axis_tdata[0]),
        .I1(s00_axis_tdata[1]),
        .I2(s00_axis_tdata[2]),
        .O(\result0[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \result0[3]_i_1 
       (.I0(s00_axis_tdata[1]),
        .I1(s00_axis_tdata[0]),
        .I2(s00_axis_tdata[2]),
        .I3(s00_axis_tdata[3]),
        .O(\result0[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \result0[4]_i_1 
       (.I0(s00_axis_tdata[2]),
        .I1(s00_axis_tdata[0]),
        .I2(s00_axis_tdata[1]),
        .I3(s00_axis_tdata[3]),
        .I4(s00_axis_tdata[4]),
        .O(\result0[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \result0[5]_i_1 
       (.I0(s00_axis_tdata[3]),
        .I1(s00_axis_tdata[1]),
        .I2(s00_axis_tdata[0]),
        .I3(s00_axis_tdata[2]),
        .I4(s00_axis_tdata[4]),
        .I5(s00_axis_tdata[5]),
        .O(\result0[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \result0[6]_i_1 
       (.I0(\result0[7]_i_2_n_0 ),
        .I1(s00_axis_tdata[6]),
        .O(\result0[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \result0[7]_i_1 
       (.I0(\result0[7]_i_2_n_0 ),
        .I1(s00_axis_tdata[6]),
        .I2(s00_axis_tdata[7]),
        .O(\result0[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \result0[7]_i_2 
       (.I0(s00_axis_tdata[5]),
        .I1(s00_axis_tdata[3]),
        .I2(s00_axis_tdata[1]),
        .I3(s00_axis_tdata[0]),
        .I4(s00_axis_tdata[2]),
        .I5(s00_axis_tdata[4]),
        .O(\result0[7]_i_2_n_0 ));
  FDRE \result0_reg[0] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result0[0]_i_1_n_0 ),
        .Q(Q[0]),
        .R(1'b0));
  FDRE \result0_reg[1] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result0[1]_i_1_n_0 ),
        .Q(Q[1]),
        .R(1'b0));
  FDRE \result0_reg[2] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result0[2]_i_1_n_0 ),
        .Q(Q[2]),
        .R(1'b0));
  FDRE \result0_reg[3] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result0[3]_i_1_n_0 ),
        .Q(Q[3]),
        .R(1'b0));
  FDRE \result0_reg[4] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result0[4]_i_1_n_0 ),
        .Q(Q[4]),
        .R(1'b0));
  FDRE \result0_reg[5] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result0[5]_i_1_n_0 ),
        .Q(Q[5]),
        .R(1'b0));
  FDRE \result0_reg[6] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result0[6]_i_1_n_0 ),
        .Q(Q[6]),
        .R(1'b0));
  FDRE \result0_reg[7] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result0[7]_i_1_n_0 ),
        .Q(Q[7]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \result1[0]_i_1 
       (.I0(s00_axis_tdata[8]),
        .O(\result1[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \result1[1]_i_1 
       (.I0(s00_axis_tdata[8]),
        .I1(s00_axis_tdata[9]),
        .O(\result1[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \result1[2]_i_1 
       (.I0(s00_axis_tdata[8]),
        .I1(s00_axis_tdata[9]),
        .I2(s00_axis_tdata[10]),
        .O(\result1[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \result1[3]_i_1 
       (.I0(s00_axis_tdata[9]),
        .I1(s00_axis_tdata[8]),
        .I2(s00_axis_tdata[10]),
        .I3(s00_axis_tdata[11]),
        .O(\result1[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \result1[4]_i_1 
       (.I0(s00_axis_tdata[10]),
        .I1(s00_axis_tdata[8]),
        .I2(s00_axis_tdata[9]),
        .I3(s00_axis_tdata[11]),
        .I4(s00_axis_tdata[12]),
        .O(\result1[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \result1[5]_i_1 
       (.I0(s00_axis_tdata[11]),
        .I1(s00_axis_tdata[9]),
        .I2(s00_axis_tdata[8]),
        .I3(s00_axis_tdata[10]),
        .I4(s00_axis_tdata[12]),
        .I5(s00_axis_tdata[13]),
        .O(\result1[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \result1[6]_i_1 
       (.I0(\result1[7]_i_2_n_0 ),
        .I1(s00_axis_tdata[14]),
        .O(\result1[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \result1[7]_i_1 
       (.I0(\result1[7]_i_2_n_0 ),
        .I1(s00_axis_tdata[14]),
        .I2(s00_axis_tdata[15]),
        .O(\result1[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \result1[7]_i_2 
       (.I0(s00_axis_tdata[13]),
        .I1(s00_axis_tdata[11]),
        .I2(s00_axis_tdata[9]),
        .I3(s00_axis_tdata[8]),
        .I4(s00_axis_tdata[10]),
        .I5(s00_axis_tdata[12]),
        .O(\result1[7]_i_2_n_0 ));
  FDRE \result1_reg[0] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result1[0]_i_1_n_0 ),
        .Q(Q[8]),
        .R(1'b0));
  FDRE \result1_reg[1] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result1[1]_i_1_n_0 ),
        .Q(Q[9]),
        .R(1'b0));
  FDRE \result1_reg[2] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result1[2]_i_1_n_0 ),
        .Q(Q[10]),
        .R(1'b0));
  FDRE \result1_reg[3] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result1[3]_i_1_n_0 ),
        .Q(Q[11]),
        .R(1'b0));
  FDRE \result1_reg[4] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result1[4]_i_1_n_0 ),
        .Q(Q[12]),
        .R(1'b0));
  FDRE \result1_reg[5] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result1[5]_i_1_n_0 ),
        .Q(Q[13]),
        .R(1'b0));
  FDRE \result1_reg[6] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result1[6]_i_1_n_0 ),
        .Q(Q[14]),
        .R(1'b0));
  FDRE \result1_reg[7] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result1[7]_i_1_n_0 ),
        .Q(Q[15]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \result2[0]_i_1 
       (.I0(s00_axis_tdata[16]),
        .O(\result2[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \result2[1]_i_1 
       (.I0(s00_axis_tdata[16]),
        .I1(s00_axis_tdata[17]),
        .O(\result2[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \result2[2]_i_1 
       (.I0(s00_axis_tdata[16]),
        .I1(s00_axis_tdata[17]),
        .I2(s00_axis_tdata[18]),
        .O(\result2[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \result2[3]_i_1 
       (.I0(s00_axis_tdata[17]),
        .I1(s00_axis_tdata[16]),
        .I2(s00_axis_tdata[18]),
        .I3(s00_axis_tdata[19]),
        .O(\result2[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \result2[4]_i_1 
       (.I0(s00_axis_tdata[18]),
        .I1(s00_axis_tdata[16]),
        .I2(s00_axis_tdata[17]),
        .I3(s00_axis_tdata[19]),
        .I4(s00_axis_tdata[20]),
        .O(\result2[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \result2[5]_i_1 
       (.I0(s00_axis_tdata[19]),
        .I1(s00_axis_tdata[17]),
        .I2(s00_axis_tdata[16]),
        .I3(s00_axis_tdata[18]),
        .I4(s00_axis_tdata[20]),
        .I5(s00_axis_tdata[21]),
        .O(\result2[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \result2[6]_i_1 
       (.I0(\result2[7]_i_2_n_0 ),
        .I1(s00_axis_tdata[22]),
        .O(\result2[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \result2[7]_i_1 
       (.I0(\result2[7]_i_2_n_0 ),
        .I1(s00_axis_tdata[22]),
        .I2(s00_axis_tdata[23]),
        .O(\result2[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \result2[7]_i_2 
       (.I0(s00_axis_tdata[21]),
        .I1(s00_axis_tdata[19]),
        .I2(s00_axis_tdata[17]),
        .I3(s00_axis_tdata[16]),
        .I4(s00_axis_tdata[18]),
        .I5(s00_axis_tdata[20]),
        .O(\result2[7]_i_2_n_0 ));
  FDRE \result2_reg[0] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result2[0]_i_1_n_0 ),
        .Q(Q[16]),
        .R(1'b0));
  FDRE \result2_reg[1] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result2[1]_i_1_n_0 ),
        .Q(Q[17]),
        .R(1'b0));
  FDRE \result2_reg[2] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result2[2]_i_1_n_0 ),
        .Q(Q[18]),
        .R(1'b0));
  FDRE \result2_reg[3] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result2[3]_i_1_n_0 ),
        .Q(Q[19]),
        .R(1'b0));
  FDRE \result2_reg[4] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result2[4]_i_1_n_0 ),
        .Q(Q[20]),
        .R(1'b0));
  FDRE \result2_reg[5] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result2[5]_i_1_n_0 ),
        .Q(Q[21]),
        .R(1'b0));
  FDRE \result2_reg[6] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result2[6]_i_1_n_0 ),
        .Q(Q[22]),
        .R(1'b0));
  FDRE \result2_reg[7] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result2[7]_i_1_n_0 ),
        .Q(Q[23]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \result3[0]_i_1 
       (.I0(s00_axis_tdata[24]),
        .O(\result3[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \result3[1]_i_1 
       (.I0(s00_axis_tdata[24]),
        .I1(s00_axis_tdata[25]),
        .O(\result3[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \result3[2]_i_1 
       (.I0(s00_axis_tdata[24]),
        .I1(s00_axis_tdata[25]),
        .I2(s00_axis_tdata[26]),
        .O(\result3[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \result3[3]_i_1 
       (.I0(s00_axis_tdata[25]),
        .I1(s00_axis_tdata[24]),
        .I2(s00_axis_tdata[26]),
        .I3(s00_axis_tdata[27]),
        .O(\result3[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \result3[4]_i_1 
       (.I0(s00_axis_tdata[26]),
        .I1(s00_axis_tdata[24]),
        .I2(s00_axis_tdata[25]),
        .I3(s00_axis_tdata[27]),
        .I4(s00_axis_tdata[28]),
        .O(\result3[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \result3[5]_i_1 
       (.I0(s00_axis_tdata[27]),
        .I1(s00_axis_tdata[25]),
        .I2(s00_axis_tdata[24]),
        .I3(s00_axis_tdata[26]),
        .I4(s00_axis_tdata[28]),
        .I5(s00_axis_tdata[29]),
        .O(\result3[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \result3[6]_i_1 
       (.I0(\result3[7]_i_2_n_0 ),
        .I1(s00_axis_tdata[30]),
        .O(\result3[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \result3[7]_i_1 
       (.I0(\result3[7]_i_2_n_0 ),
        .I1(s00_axis_tdata[30]),
        .I2(s00_axis_tdata[31]),
        .O(\result3[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \result3[7]_i_2 
       (.I0(s00_axis_tdata[29]),
        .I1(s00_axis_tdata[27]),
        .I2(s00_axis_tdata[25]),
        .I3(s00_axis_tdata[24]),
        .I4(s00_axis_tdata[26]),
        .I5(s00_axis_tdata[28]),
        .O(\result3[7]_i_2_n_0 ));
  FDRE \result3_reg[0] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result3[0]_i_1_n_0 ),
        .Q(Q[24]),
        .R(1'b0));
  FDRE \result3_reg[1] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result3[1]_i_1_n_0 ),
        .Q(Q[25]),
        .R(1'b0));
  FDRE \result3_reg[2] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result3[2]_i_1_n_0 ),
        .Q(Q[26]),
        .R(1'b0));
  FDRE \result3_reg[3] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result3[3]_i_1_n_0 ),
        .Q(Q[27]),
        .R(1'b0));
  FDRE \result3_reg[4] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result3[4]_i_1_n_0 ),
        .Q(Q[28]),
        .R(1'b0));
  FDRE \result3_reg[5] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result3[5]_i_1_n_0 ),
        .Q(Q[29]),
        .R(1'b0));
  FDRE \result3_reg[6] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result3[6]_i_1_n_0 ),
        .Q(Q[30]),
        .R(1'b0));
  FDRE \result3_reg[7] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\result3[7]_i_1_n_0 ),
        .Q(Q[31]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0
   (s00_axis_tready,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tlast,
    s00_axis_aclk,
    s00_axis_tdata,
    s00_axis_tvalid,
    m00_axis_aresetn,
    m00_axis_tready,
    s00_axis_tlast);
  output s00_axis_tready;
  output m00_axis_tvalid;
  output [31:0]m00_axis_tdata;
  output m00_axis_tlast;
  input s00_axis_aclk;
  input [31:0]s00_axis_tdata;
  input s00_axis_tvalid;
  input m00_axis_aresetn;
  input m00_axis_tready;
  input s00_axis_tlast;

  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tready;
  wire m00_axis_tvalid;
  wire p_0_in;
  wire [31:0]procWireM;
  wire s00_axis_aclk;
  wire [31:0]s00_axis_tdata;
  wire s00_axis_tlast;
  wire s00_axis_tready;
  wire s00_axis_tvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_M00_AXIS axis_test_v1_0_M00_AXIS_inst
       (.Q(procWireM),
        .SR(p_0_in),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tready(m00_axis_tready),
        .m00_axis_tvalid(m00_axis_tvalid),
        .s00_axis_aclk(s00_axis_aclk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_S00_AXIS axis_test_v1_0_S00_AXIS_inst
       (.Q(procWireM),
        .SR(p_0_in),
        .s00_axis_aclk(s00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tlast(s00_axis_tlast),
        .s00_axis_tready(s00_axis_tready),
        .s00_axis_tvalid(s00_axis_tvalid));
  FDRE \procDataM_reg[0] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[0]),
        .R(1'b0));
  FDRE \procDataM_reg[10] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[10]),
        .R(1'b0));
  FDRE \procDataM_reg[11] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[11]),
        .R(1'b0));
  FDRE \procDataM_reg[12] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[12]),
        .R(1'b0));
  FDRE \procDataM_reg[13] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[13]),
        .R(1'b0));
  FDRE \procDataM_reg[14] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[14]),
        .R(1'b0));
  FDRE \procDataM_reg[15] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[15]),
        .R(1'b0));
  FDRE \procDataM_reg[16] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[16]),
        .R(1'b0));
  FDRE \procDataM_reg[17] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[17]),
        .R(1'b0));
  FDRE \procDataM_reg[18] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[18]),
        .R(1'b0));
  FDRE \procDataM_reg[19] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[19]),
        .R(1'b0));
  FDRE \procDataM_reg[1] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[1]),
        .R(1'b0));
  FDRE \procDataM_reg[20] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[20]),
        .R(1'b0));
  FDRE \procDataM_reg[21] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[21]),
        .R(1'b0));
  FDRE \procDataM_reg[22] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[22]),
        .R(1'b0));
  FDRE \procDataM_reg[23] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[23]),
        .R(1'b0));
  FDRE \procDataM_reg[24] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[24]),
        .R(1'b0));
  FDRE \procDataM_reg[25] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[25]),
        .R(1'b0));
  FDRE \procDataM_reg[26] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[26]),
        .R(1'b0));
  FDRE \procDataM_reg[27] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[27]),
        .R(1'b0));
  FDRE \procDataM_reg[28] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[28]),
        .R(1'b0));
  FDRE \procDataM_reg[29] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[29]),
        .R(1'b0));
  FDRE \procDataM_reg[2] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[2]),
        .R(1'b0));
  FDRE \procDataM_reg[30] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[30]),
        .R(1'b0));
  FDRE \procDataM_reg[31] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[31]),
        .R(1'b0));
  FDRE \procDataM_reg[3] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[3]),
        .R(1'b0));
  FDRE \procDataM_reg[4] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[4]),
        .R(1'b0));
  FDRE \procDataM_reg[5] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[5]),
        .R(1'b0));
  FDRE \procDataM_reg[6] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[6]),
        .R(1'b0));
  FDRE \procDataM_reg[7] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[7]),
        .R(1'b0));
  FDRE \procDataM_reg[8] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[8]),
        .R(1'b0));
  FDRE \procDataM_reg[9] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(1'b0),
        .Q(procWireM[9]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_M00_AXIS
   (m00_axis_tvalid,
    SR,
    m00_axis_tlast,
    m00_axis_tdata,
    s00_axis_aclk,
    Q,
    m00_axis_aresetn,
    m00_axis_tready);
  output m00_axis_tvalid;
  output [0:0]SR;
  output m00_axis_tlast;
  output [31:0]m00_axis_tdata;
  input s00_axis_aclk;
  input [31:0]Q;
  input m00_axis_aresetn;
  input m00_axis_tready;

  wire [31:0]Q;
  wire [0:0]SR;
  wire axis_tlast;
  wire axis_tvalid;
  wire count0;
  wire \count[3]_i_1_n_0 ;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tready;
  wire m00_axis_tvalid;
  wire [1:0]mst_exec_state;
  wire \mst_exec_state[0]_i_1_n_0 ;
  wire \mst_exec_state[1]_i_1_n_0 ;
  wire \mst_exec_state[1]_i_2_n_0 ;
  wire [4:0]p_0_in__0;
  wire read_pointer;
  wire \read_pointer[0]_i_1_n_0 ;
  wire \read_pointer[1]_i_1_n_0 ;
  wire \read_pointer[2]_i_1_n_0 ;
  wire \read_pointer[3]_i_2_n_0 ;
  wire [3:0]read_pointer_reg__0;
  wire s00_axis_aclk;
  wire [4:0]sel0;
  wire \stream_data_out[0]_i_1_n_0 ;
  wire \stream_data_out[1]_i_1_n_0 ;
  wire \stream_data_out[2]_i_1_n_0 ;
  wire \stream_data_out[31]_i_1_n_0 ;
  wire \stream_data_out[31]_i_2_n_0 ;
  wire \stream_data_out[3]_i_1_n_0 ;
  wire \stream_data_out[4]_i_1_n_0 ;
  wire tx_done_i_1_n_0;
  wire tx_done_i_2_n_0;
  wire tx_done_reg_n_0;

  LUT4 #(
    .INIT(16'h4000)) 
    axis_tlast_delay_i_1
       (.I0(read_pointer_reg__0[3]),
        .I1(read_pointer_reg__0[2]),
        .I2(read_pointer_reg__0[0]),
        .I3(read_pointer_reg__0[1]),
        .O(axis_tlast));
  FDRE axis_tlast_delay_reg
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(axis_tlast),
        .Q(m00_axis_tlast),
        .R(SR));
  LUT3 #(
    .INIT(8'h04)) 
    axis_tvalid_delay_i_1
       (.I0(mst_exec_state[0]),
        .I1(mst_exec_state[1]),
        .I2(read_pointer_reg__0[3]),
        .O(axis_tvalid));
  FDRE axis_tvalid_delay_reg
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(axis_tvalid),
        .Q(m00_axis_tvalid),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1 
       (.I0(sel0[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count[1]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \count[2]_i_1 
       (.I0(sel0[2]),
        .I1(sel0[0]),
        .I2(sel0[1]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count[3]_i_1 
       (.I0(sel0[2]),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .I3(sel0[3]),
        .O(\count[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h44044444)) 
    \count[4]_i_1 
       (.I0(mst_exec_state[1]),
        .I1(mst_exec_state[0]),
        .I2(sel0[3]),
        .I3(\mst_exec_state[1]_i_2_n_0 ),
        .I4(sel0[4]),
        .O(count0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count[4]_i_2 
       (.I0(sel0[0]),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(sel0[3]),
        .I4(sel0[4]),
        .O(p_0_in__0[4]));
  FDRE \count_reg[0] 
       (.C(s00_axis_aclk),
        .CE(count0),
        .D(p_0_in__0[0]),
        .Q(sel0[0]),
        .R(SR));
  FDRE \count_reg[1] 
       (.C(s00_axis_aclk),
        .CE(count0),
        .D(p_0_in__0[1]),
        .Q(sel0[1]),
        .R(SR));
  FDRE \count_reg[2] 
       (.C(s00_axis_aclk),
        .CE(count0),
        .D(p_0_in__0[2]),
        .Q(sel0[2]),
        .R(SR));
  FDRE \count_reg[3] 
       (.C(s00_axis_aclk),
        .CE(count0),
        .D(\count[3]_i_1_n_0 ),
        .Q(sel0[3]),
        .R(SR));
  FDRE \count_reg[4] 
       (.C(s00_axis_aclk),
        .CE(count0),
        .D(p_0_in__0[4]),
        .Q(sel0[4]),
        .R(SR));
  LUT5 #(
    .INIT(32'hFF00BFFF)) 
    \mst_exec_state[0]_i_1 
       (.I0(\mst_exec_state[1]_i_2_n_0 ),
        .I1(sel0[4]),
        .I2(sel0[3]),
        .I3(mst_exec_state[0]),
        .I4(mst_exec_state[1]),
        .O(\mst_exec_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF555530000000)) 
    \mst_exec_state[1]_i_1 
       (.I0(tx_done_reg_n_0),
        .I1(\mst_exec_state[1]_i_2_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(mst_exec_state[0]),
        .I5(mst_exec_state[1]),
        .O(\mst_exec_state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \mst_exec_state[1]_i_2 
       (.I0(sel0[0]),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .O(\mst_exec_state[1]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    mst_exec_state_i_1
       (.I0(m00_axis_aresetn),
        .O(SR));
  FDRE \mst_exec_state_reg[0] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\mst_exec_state[0]_i_1_n_0 ),
        .Q(mst_exec_state[0]),
        .R(SR));
  FDRE \mst_exec_state_reg[1] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\mst_exec_state[1]_i_1_n_0 ),
        .Q(mst_exec_state[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \read_pointer[0]_i_1 
       (.I0(read_pointer_reg__0[0]),
        .O(\read_pointer[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \read_pointer[1]_i_1 
       (.I0(read_pointer_reg__0[0]),
        .I1(read_pointer_reg__0[1]),
        .O(\read_pointer[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \read_pointer[2]_i_1 
       (.I0(read_pointer_reg__0[0]),
        .I1(read_pointer_reg__0[1]),
        .I2(read_pointer_reg__0[2]),
        .O(\read_pointer[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0020)) 
    \read_pointer[3]_i_1 
       (.I0(m00_axis_tready),
        .I1(read_pointer_reg__0[3]),
        .I2(mst_exec_state[1]),
        .I3(mst_exec_state[0]),
        .O(read_pointer));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \read_pointer[3]_i_2 
       (.I0(read_pointer_reg__0[1]),
        .I1(read_pointer_reg__0[0]),
        .I2(read_pointer_reg__0[2]),
        .O(\read_pointer[3]_i_2_n_0 ));
  FDRE \read_pointer_reg[0] 
       (.C(s00_axis_aclk),
        .CE(read_pointer),
        .D(\read_pointer[0]_i_1_n_0 ),
        .Q(read_pointer_reg__0[0]),
        .R(SR));
  FDRE \read_pointer_reg[1] 
       (.C(s00_axis_aclk),
        .CE(read_pointer),
        .D(\read_pointer[1]_i_1_n_0 ),
        .Q(read_pointer_reg__0[1]),
        .R(SR));
  FDRE \read_pointer_reg[2] 
       (.C(s00_axis_aclk),
        .CE(read_pointer),
        .D(\read_pointer[2]_i_1_n_0 ),
        .Q(read_pointer_reg__0[2]),
        .R(SR));
  FDRE \read_pointer_reg[3] 
       (.C(s00_axis_aclk),
        .CE(read_pointer),
        .D(\read_pointer[3]_i_2_n_0 ),
        .Q(read_pointer_reg__0[3]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \stream_data_out[0]_i_1 
       (.I0(read_pointer_reg__0[0]),
        .I1(m00_axis_aresetn),
        .I2(Q[0]),
        .O(\stream_data_out[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h6F60)) 
    \stream_data_out[1]_i_1 
       (.I0(read_pointer_reg__0[0]),
        .I1(read_pointer_reg__0[1]),
        .I2(m00_axis_aresetn),
        .I3(Q[1]),
        .O(\stream_data_out[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h78FF7800)) 
    \stream_data_out[2]_i_1 
       (.I0(read_pointer_reg__0[0]),
        .I1(read_pointer_reg__0[1]),
        .I2(read_pointer_reg__0[2]),
        .I3(m00_axis_aresetn),
        .I4(Q[2]),
        .O(\stream_data_out[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000800)) 
    \stream_data_out[31]_i_1 
       (.I0(m00_axis_aresetn),
        .I1(m00_axis_tready),
        .I2(read_pointer_reg__0[3]),
        .I3(mst_exec_state[1]),
        .I4(mst_exec_state[0]),
        .O(\stream_data_out[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h0400FFFF)) 
    \stream_data_out[31]_i_2 
       (.I0(mst_exec_state[0]),
        .I1(mst_exec_state[1]),
        .I2(read_pointer_reg__0[3]),
        .I3(m00_axis_tready),
        .I4(m00_axis_aresetn),
        .O(\stream_data_out[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h7F80FFFF7F800000)) 
    \stream_data_out[3]_i_1 
       (.I0(read_pointer_reg__0[1]),
        .I1(read_pointer_reg__0[0]),
        .I2(read_pointer_reg__0[2]),
        .I3(read_pointer_reg__0[3]),
        .I4(m00_axis_aresetn),
        .I5(Q[3]),
        .O(\stream_data_out[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000FFFF80000000)) 
    \stream_data_out[4]_i_1 
       (.I0(read_pointer_reg__0[2]),
        .I1(read_pointer_reg__0[0]),
        .I2(read_pointer_reg__0[1]),
        .I3(read_pointer_reg__0[3]),
        .I4(m00_axis_aresetn),
        .I5(Q[4]),
        .O(\stream_data_out[4]_i_1_n_0 ));
  FDRE \stream_data_out_reg[0] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(\stream_data_out[0]_i_1_n_0 ),
        .Q(m00_axis_tdata[0]),
        .R(1'b0));
  FDRE \stream_data_out_reg[10] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[10]),
        .Q(m00_axis_tdata[10]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[11] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[11]),
        .Q(m00_axis_tdata[11]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[12] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[12]),
        .Q(m00_axis_tdata[12]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[13] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[13]),
        .Q(m00_axis_tdata[13]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[14] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[14]),
        .Q(m00_axis_tdata[14]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[15] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[15]),
        .Q(m00_axis_tdata[15]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[16] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[16]),
        .Q(m00_axis_tdata[16]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[17] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[17]),
        .Q(m00_axis_tdata[17]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[18] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[18]),
        .Q(m00_axis_tdata[18]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[19] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[19]),
        .Q(m00_axis_tdata[19]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[1] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(\stream_data_out[1]_i_1_n_0 ),
        .Q(m00_axis_tdata[1]),
        .R(1'b0));
  FDRE \stream_data_out_reg[20] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[20]),
        .Q(m00_axis_tdata[20]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[21] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[21]),
        .Q(m00_axis_tdata[21]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[22] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[22]),
        .Q(m00_axis_tdata[22]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[23] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[23]),
        .Q(m00_axis_tdata[23]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[24] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[24]),
        .Q(m00_axis_tdata[24]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[25] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[25]),
        .Q(m00_axis_tdata[25]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[26] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[26]),
        .Q(m00_axis_tdata[26]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[27] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[27]),
        .Q(m00_axis_tdata[27]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[28] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[28]),
        .Q(m00_axis_tdata[28]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[29] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[29]),
        .Q(m00_axis_tdata[29]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[2] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(\stream_data_out[2]_i_1_n_0 ),
        .Q(m00_axis_tdata[2]),
        .R(1'b0));
  FDRE \stream_data_out_reg[30] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[30]),
        .Q(m00_axis_tdata[30]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[31] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[31]),
        .Q(m00_axis_tdata[31]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[3] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(\stream_data_out[3]_i_1_n_0 ),
        .Q(m00_axis_tdata[3]),
        .R(1'b0));
  FDRE \stream_data_out_reg[4] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(\stream_data_out[4]_i_1_n_0 ),
        .Q(m00_axis_tdata[4]),
        .R(1'b0));
  FDRE \stream_data_out_reg[5] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[5]),
        .Q(m00_axis_tdata[5]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[6] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[6]),
        .Q(m00_axis_tdata[6]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[7] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[7]),
        .Q(m00_axis_tdata[7]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[8] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[8]),
        .Q(m00_axis_tdata[8]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  FDRE \stream_data_out_reg[9] 
       (.C(s00_axis_aclk),
        .CE(\stream_data_out[31]_i_2_n_0 ),
        .D(Q[9]),
        .Q(m00_axis_tdata[9]),
        .R(\stream_data_out[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAA8AAAAA)) 
    tx_done_i_1
       (.I0(tx_done_i_2_n_0),
        .I1(mst_exec_state[0]),
        .I2(mst_exec_state[1]),
        .I3(read_pointer_reg__0[3]),
        .I4(m00_axis_tready),
        .O(tx_done_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAABAAAA00000000)) 
    tx_done_i_2
       (.I0(tx_done_reg_n_0),
        .I1(read_pointer_reg__0[1]),
        .I2(read_pointer_reg__0[0]),
        .I3(read_pointer_reg__0[2]),
        .I4(read_pointer_reg__0[3]),
        .I5(m00_axis_aresetn),
        .O(tx_done_i_2_n_0));
  FDRE tx_done_reg
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(tx_done_i_1_n_0),
        .Q(tx_done_reg_n_0),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0_S00_AXIS
   (s00_axis_tready,
    Q,
    SR,
    s00_axis_aclk,
    s00_axis_tlast,
    s00_axis_tdata,
    s00_axis_tvalid);
  output s00_axis_tready;
  output [31:0]Q;
  input [0:0]SR;
  input s00_axis_aclk;
  input s00_axis_tlast;
  input [31:0]s00_axis_tdata;
  input s00_axis_tvalid;

  wire [31:0]Q;
  wire [0:0]SR;
  wire fifo_wren;
  wire mst_exec_state_i_2_n_0;
  wire s00_axis_aclk;
  wire [31:0]s00_axis_tdata;
  wire s00_axis_tlast;
  wire s00_axis_tready;
  wire s00_axis_tvalid;
  wire [2:0]write_pointer;
  wire \write_pointer[0]_i_1_n_0 ;
  wire \write_pointer[1]_i_1_n_0 ;
  wire \write_pointer[2]_i_1_n_0 ;
  wire writes_done;
  wire writes_done_i_1_n_0;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_add_one add_one_inst
       (.Q(Q),
        .s00_axis_aclk(s00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata));
  LUT3 #(
    .INIT(8'h74)) 
    mst_exec_state_i_2
       (.I0(writes_done),
        .I1(s00_axis_tready),
        .I2(s00_axis_tvalid),
        .O(mst_exec_state_i_2_n_0));
  FDRE mst_exec_state_reg
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(mst_exec_state_i_2_n_0),
        .Q(s00_axis_tready),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \write_pointer[0]_i_1 
       (.I0(s00_axis_tready),
        .I1(s00_axis_tvalid),
        .I2(write_pointer[0]),
        .O(\write_pointer[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \write_pointer[1]_i_1 
       (.I0(write_pointer[0]),
        .I1(s00_axis_tvalid),
        .I2(s00_axis_tready),
        .I3(write_pointer[1]),
        .O(\write_pointer[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \write_pointer[2]_i_1 
       (.I0(write_pointer[0]),
        .I1(write_pointer[1]),
        .I2(s00_axis_tvalid),
        .I3(s00_axis_tready),
        .I4(write_pointer[2]),
        .O(\write_pointer[2]_i_1_n_0 ));
  FDRE \write_pointer_reg[0] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\write_pointer[0]_i_1_n_0 ),
        .Q(write_pointer[0]),
        .R(SR));
  FDRE \write_pointer_reg[1] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\write_pointer[1]_i_1_n_0 ),
        .Q(write_pointer[1]),
        .R(SR));
  FDRE \write_pointer_reg[2] 
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(\write_pointer[2]_i_1_n_0 ),
        .Q(write_pointer[2]),
        .R(SR));
  LUT6 #(
    .INIT(64'hFF80FFFFFF80FF80)) 
    writes_done_i_1
       (.I0(write_pointer[1]),
        .I1(write_pointer[0]),
        .I2(write_pointer[2]),
        .I3(s00_axis_tlast),
        .I4(fifo_wren),
        .I5(writes_done),
        .O(writes_done_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    writes_done_i_2
       (.I0(s00_axis_tvalid),
        .I1(s00_axis_tready),
        .O(fifo_wren));
  FDRE writes_done_reg
       (.C(s00_axis_aclk),
        .CE(1'b1),
        .D(writes_done_i_1_n_0),
        .Q(writes_done),
        .R(SR));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_axis_test_0_1,axis_test_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "axis_test_v1_0,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s00_axis_tdata,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid,
    s00_axis_tready,
    s00_axis_aclk,
    s00_axis_aresetn,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tvalid,
    m00_axis_tready,
    m00_axis_aclk,
    m00_axis_aresetn);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TDATA" *) input [31:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TSTRB" *) input [3:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TVALID" *) input s00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_CLK, ASSOCIATED_BUSIF S00_AXIS, ASSOCIATED_RESET s00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) input s00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_RST, POLARITY ACTIVE_LOW" *) input s00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW" *) input m00_axis_aresetn;

  wire \<const1> ;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tready;
  wire m00_axis_tvalid;
  wire s00_axis_aclk;
  wire [31:0]s00_axis_tdata;
  wire s00_axis_tlast;
  wire s00_axis_tready;
  wire s00_axis_tvalid;

  assign m00_axis_aresetn = s00_axis_aresetn;
  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  assign s00_axis_aclk = m00_axis_aclk;
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_test_v1_0 inst
       (.m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tready(m00_axis_tready),
        .m00_axis_tvalid(m00_axis_tvalid),
        .s00_axis_aclk(s00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tlast(s00_axis_tlast),
        .s00_axis_tready(s00_axis_tready),
        .s00_axis_tvalid(s00_axis_tvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
